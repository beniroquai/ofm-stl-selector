import Vue from "vue";
import VueRouter from "vue-router";
import SuiVue from "semantic-ui-vue";
import VueClipboard from "vue-clipboard2";
import StlSelector from "./views/StlSelector.vue";

VueClipboard.config.autoSetContainer = true;
Vue.config.productionTip = false;
Vue.use(VueClipboard);
Vue.use(SuiVue);
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/v6.1.2"
  },
  {
    path: "/:buildVersion",
    name: "stl-selector",
    component: StlSelector
  }
];

const router = new VueRouter({
  routes
});

const App = {
  template: "<router-view />"
};

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
