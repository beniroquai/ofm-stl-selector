const _ = require("lodash");

module.exports = function computeStls(allStls, optionData) {
  let stls = allStls.map(v => v.stl);
  for (const v of allStls) {
    for (const param in v.parameters) {
      if (param in optionData) {
        const o = optionData[param];
        const p = v.parameters[param];
        if ((_.isArray(p) && !p.includes(o)) || (!_.isArray(p) && p !== o)) {
          stls = removeOne(stls, v.stl);
          break;
        }
      }
    }
  }
  return _.uniq(stls);
};

function removeOne(arr, value) {
  var index = arr.indexOf(value);
  if (index > -1) {
    arr.splice(index, 1);
  }
  return arr;
}
